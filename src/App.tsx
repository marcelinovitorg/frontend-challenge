import React, {
    useState
} from 'react';
import './styles/Style.scss';
import catalogo from "./data/catalogo.json";

interface Catalogo {
    items?: Items[];
    productName?: string[];

}
interface Items {
    sellers: Sellers[];
}
interface Sellers {
    installments: Installments[];
}
interface Installments {
    value: number;
}

const App: React.FC = () => {

    const [inputName, setInputname] = useState("");
    const [inputEmail, setInputemail] = useState("");

    function AlertEmail() {
        if ((inputName !== "") && (inputEmail !== "")) {
            return alert("Fórmulario enviado com sucesso!");
        } else return alert("É presico informar seu e-mail e nome!");
    }
    return (
        <div>
            <div id="container-vitrine">
                {catalogo && catalogo.map(catalogo => (
                    <div id="container-card-info">
                        {catalogo.itemMetadata?.items && catalogo.itemMetadata?.items.map(items => (
                            <img id="image-clock" src={items.MainImage}></img>
                        ))}
                        <h1 id="title-clock">{(catalogo.productName).toUpperCase()}</h1>
                        <button id="btn-comprar" value="text" type="submit">COMPRAR</button>
                    </div>
                ))}
            </div>
            <div id="container-newsletter">

                <h1 id="title-newsletter">ASSINE NOSSA NEWSLETTER</h1>
                <h3 id="sub-title-newsletter">Fique por dentro das nossas novidades e receba 10% de desconto na primeira compra.</h3>
                <h2 id="warning-title-newsletter">* valido somente para joias e não acumulativo com outras promoções</h2>
                <div id="container-input">
                    <input id="input-name" placeholder="Seu nome" type="text" onChange={name => setInputname(name.target.value)} ></input>
                    <input id="input-email" placeholder="Seu e-mail" type="email" onChange={email => setInputemail(email.target.value)}></input>
                    <button id="btn-enviar" onClick={AlertEmail}>Enviar</button>
                </div>
            </div>
        </div>


    )

}
export default App;
